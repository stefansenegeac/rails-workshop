module API
  module V1
    class Products < Grape::API

      helpers do
        def auth
          error!('Unauthorized', 401) unless (headers['App-Key'] == AppCredentials::APP_KEY) && (headers['App-Id'] == AppCredentials::APP_ID)
        end
      end
      before do
          auth
      end

      format :json
      resource :products do
        desc "Return all products"
        get "", root: :products do
          Product.all
        end

        desc "Return a product"
        params do
          requires :id, type: Integer, desc: "ID of the
            product"
        end
        get ":id", root: "product" do
          Product.where(id: params[:id]).first!
        end

        desc "Delete a product"
        params do
          requires :id, type: Integer, desc: "ID of the product"
        end
        delete ":id", root: "product" do
          Product.find(params[:id]).destroy
        end

        desc "Create a new product"
        params do
          requires :name, type: String
          requires :price, type: Integer
          requires :old_price, type: Integer
          requires :short_description, type: String
          requires :full_description, type: String
        end
        post do
          Product.create!({
                              name:params[:name],
                              price:params[:price],
                              old_price:params[:old_price],
                              short_description:params[:short_description],
                              full_description:params[:full_description]
                          })
        end


        desc "Update a product"
        params do
          requires :price, type: Integer
          optional :short_description, type: String
          optional :full_description, type: String
        end
        put ":id" do
          a = Product.find(params[:id])
          a.update!({
                                               price:params[:price],
                                               short_description:params[:short_description],
                                               full_description:params[:full_description]
                                           })
        end
      end
    end
  end
end