class ProductsController < ApplicationController
  before_action :find_product, only: [:show, :edit, :update, :destroy]

  def index
    @products = Product.all
  end

  def show
    @product = Product.find(params[:id])
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      redirect_to products_path, :notice => "Your product has been saved."
    else
      render "new"
    end
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    if @product.update(product_params)
      redirect_to product_path(@product), :notice => "Your product has been updated!"
    else
      render 'edit'
    end
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    redirect_to products_path, :notice => "Your product has been deleted."
  end

  private
    def product_params
      params.require(:product).permit(:name, :price, :old_price, :short_description, :full_description)
    end

    def find_product
      @product = Product.find(params[:id])
    end
end
