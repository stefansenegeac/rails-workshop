class Product < ActiveRecord::Base
  validates_presence_of :name, :short_description, :price, :old_price, :full_description
end
